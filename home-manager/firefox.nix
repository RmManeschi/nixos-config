{ pkgs, ... }: {
  programs.firefox = {
    enable = true;
    profiles = {
      default = {
        id = 0;
        settings = {
          "browser.startup.homepage" = "about:blank";
          "browser.urlbar.placeholderName" = "Google";
        };
      };
    };
  };
}